# Soal-Shift-Sisop-Modul-4-C11-2022

## Daftar Isi ##
- [Daftar Anggota Kelompok](#daftar-anggota-kelompok)
- [Nomer 1](#nomer-1)
    - [Soal  1.A](#soal-1a)
- [Nomer 2](#nomer-2)
    - [Soal  2.a](#soal-2a)
    - [Soal  2.C](#soal-2c)
- [Note](#note)


## Daftar Anggota Kelompok ##

NRP | NAMA | KELAS
--- | --- | ---
5025201094  | Reyner Fernaldi | SISOP C

## Nomer 1 ##
### Soal 1.a ###

Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

```c
void encrypt_atbash_rot(char *str)
{
  // Tidak perllu di encrypt
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;

  char extension[10];
  char fname[1000];
  int count_fname = 0, count_ext = 0;
  bool getext=0;

  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] == '.')
    {
      fname[count_fname] = str[i];
      count_fname++;
      getext = 1;
    }
    // Mengambil extensi (karena ekstensi tidak terenkripsi)
    if (getext == 1){
      extension[count_ext] = str[i];
      count_ext++;
      }
    else{
      fname[count_fname] = str[i];
      count_fname++;
      }
  }

  for (int i = 0; i < count_fname; i++)
  {
    // Atbash
    if (isupper(fname[i]))
      fname[i] = 'A' + 'Z' - fname[i];
    // Rot13
    else if (islower(fname[i]))
    {
      if (fname[i] > 109)
        fname[i] -= 13;
      else
        fname[i] += 13;
    }
  } 
  // Gabungkan kembali dengan ekstensi
  strcat(fname, extension);
  strcpy(str, fname);
}

```
Pada fungsi diatas, terdapat beberapa variabel:
- `extension` : untuk menyimpan ekstensi
- `fname` : untuk menyimpan nama file
- `count_fname` : untuk keep track nama file.
- `count_extension` : untuk keep track extensi
- `getext` : penanda bahwa saat ini adalah string extension

Jika file berisi `.` atau `..`, maka file tidak perlu di enkripsi dan kita dapat melewatlkannya. Selanjutnya, string akan diiterasi per karakter. String akan dipisahkan menjadi nama file dan extensinya, dimana nama file akan dienkripsi sedangkan extensi tidak. Karena nama file dan ekstensi dipisahkan oleh `.`, maka kita dapat menggunakan if else dengan kondisi `.`. 

Selanjutnya jika nama file huruf besar, maka digunakan atbash cipher. atbash cipher pada dasarnya adalah cipher substitusi sederhana dengan cara membalikkan alfabet sehingga setiap huruf dipetakan ke huruf di posisi yang sama kebalikan dari abjad. Sehingga, kita dapat menggunakan
```c
fname[i] = 'A' + 'Z' - fname[i];
```
Jika huruf kecil, maka akan digunakan enkripsi rot13. rot13 adalah algoritma enkripsi sederhana yang menggunakan sandi abjad-tunggal dengan pergeseran k=13. Sehingga kita dapat menggunakan
```c
      if (fname[i] > 109)
        fname[i] -= 13;
      else
        fname[i] += 13;
```
Kemudian hasil enkripsi akan disatukan dengan ekstensinya

## Nomer 2 ##
### Soal 2.a ###
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere)

```c
void enskripsi_vigenere(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  int temp = 0;
  for (int i = 0; i < strlen(str); i++)
  {
    if (key[temp] == '\0')
      temp = 0;
    if (isupper(str[i])){
      str[i] = (str[i] - 'A' + key[temp] - 'A') % 26 + 'A';
      temp++;
      }
    else if (islower(str[i])){
      str[i] = (str[i] - 'a' + tolower(key[temp]) - 'a') % 26 + 'a';
      temp++
      }
    else
      temp = 0;
  }
}

```
Pada fungsi diatas, terdapat beberapa variabel:
- `temp` = untuk keeptrack key. variabel ini berguna untuk mengembalikan key ke indeks awal agar key terus looping
Jika file berisi `.` atau `..`, maka file tidak perlu di enkripsi dan kita dapat melewatlkannya.
Untuk mengenkripsi huruf kapital dapat menggunakan rumus
`str[i] = (str[i] - 'A' + key[temp] - 'A') % 26 + 'A'`

sedangkan untuk mengenkripsi huruf kecil dapat menggunakan rumus
`str[i] = (str[i] - 'a' + tolower(key[temp]) - 'a') % 26 + 'a';`

### Soal 2.c ###
Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

```c
void decode_vigenere(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  int temp = 0;
  for (int i = 0; i < strlen(str); i++)
  {
    if (key[temp] == '\0')
      temp = 0;
    if (isupper(str[i])){
      str[i] = (str[i] - 'A' - key[temp] + 'A' + 26) % 26 + 'A';
      temp++;
      }
    else if (islower(str[i])){
      str[i] = (str[i] - 'a' - tolower(key[temp]) + 'a' + 26) % 26 + 'a';
      temp++;
      }
    else
      temp = 0;
  }
}

```

Untuk decode, kurang lebih sama seperti encode, hanya beda operasi saja.

## Note ##
Saya hanya mengerjakan fungsi encrypt dan decrypt saja, dikarenakan saya masih bingung dengan fuse. mohon maaf mas :(







