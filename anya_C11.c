#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <fuse.h>
#include <dirent.h>
#include <stdbool.h>


char type1[10] = "Animeku_";
char type2[10] = "IAN_";
char key[15] = "INNUGANTENG";

void encrypt_atbash_rot(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;

  char extension[10];
  char fname[1000];
  int count_fname = 0, count_ext = 0;
  bool getext=0;

  for (int i = 0; i < strlen(str); i++)
  {
    if (str[i] == '/')
      continue;
    if (str[i] == '.')
    {
      fname[count_fname] = str[i];
      count_fname++;
      getext = 1;
    }
    if (getext == 1){
      extension[count_ext] = str[i];
      count_ext++;
      }
    else{
      fname[count_fname] = str[i];
      count_fname++;
      }
  }

  for (int i = 0; i < count_fname; i++)
  {
    if (isupper(fname[i]))
      fname[i] = 'A' + 'Z' - fname[i];
    else if (islower(fname[i]))
    {
      if (fname[i] > 109)
        fname[i] -= 13;
      else
        fname[i] += 13;
    }
  }

  strcat(fname, extension);
  strcpy(str, fname);
}

void enskripsi_vigenere(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  int temp = 0;
  for (int i = 0; i < strlen(str); i++)
  {
    if (key[temp] == '\0')
      temp = 0;
    if (isupper(str[i])){
      str[i] = (str[i] - 'A' + key[temp] - 'A') % 26 + 'A';
      temp++;
      }
    else if (islower(str[i])){
      str[i] = (str[i] - 'a' + tolower(key[temp]) - 'a') % 26 + 'a';
      temp++
      }
    else
      temp = 0;
  }
}

void decode_vigenere(char *str)
{
  if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
    return;
  int temp = 0;
  for (int i = 0; i < strlen(str); i++)
  {
    if (key[temp] == '\0')
      temp = 0;
    if (isupper(str[i])){
      str[i] = (str[i] - 'A' - key[temp] + 'A' + 26) % 26 + 'A';
      temp++;
      }
    else if (islower(str[i])){
      str[i] = (str[i] - 'a' - tolower(key[temp]) + 'a' + 26) % 26 + 'a';
      temp++;
      }
    else
      temp = 0;
  }
}




